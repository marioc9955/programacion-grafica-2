# DESARROLLADORES:
# -Cabrera Rojas Luis Mario                  lmcabrera@uce.edu.ec
# Nicolalde Perugachi Joana Estefanía        jenicolaldep@uce.edu.ec
# Rodríguez Espitia Luis Felipe              lfrodrigueze@uce.edu.ec
# Nasimba Caiza Michael Christian            mcnasimba@uce.edu.ec
# Columba Párraga Erick David                edcolumba@uce.edu.ec
# Fecha de última modificación:26/08/2020

import math
from Textura import *
import primitivas3D
from numpy import arange


from objloader import *
import Suelo as s


class Cono:
    def __init__(self, gl, np):
        self.gl = gl
        self.np = np

    def crearCono(self, r, g, b, radio, alto, ex, ey, ez, tx, ty, tz, rx, ry, rz, v, textura):

        self.gl.glPushMatrix()

        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glBegin(self.gl.GL_POLYGON)
        for i in self.np.arange(0, 7, 0.1):
            auxX = math.sin(i) * radio
            auxZ = math.cos(i) * radio
            self.gl.glColor3f(r * 0.8, g * 0.8, b * 0.8)
            if (textura):
                self.gl.glTexCoord2f(auxX * v, auxZ * v)  # para colocar la textura
            self.gl.glVertex(auxX, 0, auxZ)

            # self.gl.glVertex(auxX, 0, auxZ)
        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_QUAD_STRIP)

        for i in self.np.arange(0, 6.309, 0.1):
            auxX = math.sin(i) * radio
            auxZ = math.cos(i) * radio
            self.gl.glColor3f(r * 0.9, g * 0.9, b * 0.7)
            if (textura):
                self.gl.glTexCoord2f(auxX * v, 0)
            self.gl.glVertex(auxX, 0, auxZ)
            self.gl.glColor3f(r * 0.5, g * 0.5, b * 0.5)
            if (textura):
                self.gl.glTexCoord2f(0, alto * v)
            self.gl.glVertex(0, alto, 0)
        self.gl.glEnd()

        self.gl.glPopMatrix()

    def crearConoWireFrame(self, r, g, b, s, radio, alto, ex, ey, ez, tx, ty, tz, rx, ry, rz):

        self.gl.glPushMatrix()

        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        self.gl.glColor3f(r, g, b)
        self.gl.glLineWidth(s)

        self.gl.glBegin(self.gl.GL_LINE_LOOP)
        for i in self.np.arange(0, 7, 0.1):
            auxX = math.sin(i) * radio
            auxZ = math.cos(i) * radio
            self.gl.glVertex(auxX, 0, auxZ)

        self.gl.glEnd()

        self.gl.glBegin(self.gl.GL_LINE_LOOP)

        for i in self.np.arange(0, 6.309, 0.1):
            auxX = math.sin(i) * radio
            auxZ = math.cos(i) * radio
            self.gl.glVertex(auxX, 0, auxZ)
            self.gl.glVertex(0, alto, 0)
        self.gl.glEnd()

        self.gl.glPopMatrix()

class Piedra:

    def crearPiedra(self,tx,ty,tz):
     #loadTexture("texturas/piedra.jpg")
     primitivas3D.draw_sphere(1 ,1,1,1,1,1,tx,ty,tz,0.3)

class Poste:
    #Inicializador

     def crearPoste(self,radio,alto,tx,ty,tz):
         glPushMatrix()
         glTranslate(tx, ty, tz)
         primitivas3D.draw_cylinder(0.647059 ,0.164706,0.164706,radio,alto)
         glPopMatrix()

class Boton:

    def dibujarBoton(self,posx, posy, posz, ancho, alto, img):
        glPushMatrix()
        glTranslatef(posx, posy, posz)
        glScalef(ancho, alto, 2)
        # glColor(0, 1, 0)

        glBindTexture(GL_TEXTURE_2D, img)
        glBegin(GL_QUADS)
        # glColor4f(0, 1, 0, 1)
        # glColor(0,0.5,0)
        glTexCoord2f(0, 0)
        glVertex3f(-1.000000, -1.000000, -1.000000)
        glTexCoord2f(0, 1)
        glVertex3f(-1.000000, 1.000000, -1.000000)
        glTexCoord2f(1, 1)
        glVertex3f(1.000000, 1.000000, -1.000000)
        glTexCoord2f(1, 0)
        glVertex3f(1.000000, -1.000000, -1.000000)
        glEnd()
        glPopMatrix()

from Textura import *
class Esfera:
    # Inicializador
    def __init__(self, gl, glu):
        self.gl = gl
        self.glu = glu

    # Funcion para crear la esfera
    def crearEsfera(self, radio, p, q, r, g, b, ex, ey, ez, tx, ty, tz, rx, ry, rz):
        t=Textura()
        t.loadTexture("texturas/cactus.jpg")
        self.gl.glPushMatrix()
        # Transformaciones

        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)
        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)

        sphere = self.glu.gluNewQuadric()
        self.gl.glColor3f(r, g, b)
        self.glu.gluQuadricTexture(sphere, self.gl.GL_TRUE)
        self.glu.gluQuadricDrawStyle(sphere, self.glu.GLU_FILL)
        # self.gl.glTexCoord2f() #No tengo los vertices :c
        self.glu.gluSphere(sphere, radio, p, q)

        self.gl.glPopMatrix()

    def crearEsferaWireFrame(self, radio, p, q, r, g, b, s, ex, ey, ez, tx, ty, tz, rx, ry, rz):
        self.gl.glPushMatrix()

        self.gl.glScalef(ex, ey, ez)
        self.gl.glTranslate(tx, ty, tz)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        sphere = self.glu.gluNewQuadric();
        self.gl.glColor3f(r, g, b)
        self.gl.glLineWidth(s)
        self.glu.gluQuadricDrawStyle(sphere, self.glu.GLU_LINE);
        self.glu.gluSphere(sphere, radio, p, q);

        self.gl.glPopMatrix()


class Cactus:
    # Inicializador
    def __init__(self, gl, glu, posicion):
        self.gl = gl
        self.glu = glu
        self.posicion = posicion
        self.collider = ColliderElipse(0.2, 0.2, 0.5, posicion)

    # Funcion para crear un cubo
    def crearCactus(self, ex, ey, ez, rx, ry, rz):
        self.gl.glPushMatrix()
        # Transformaciones
        glTranslatef(self.posicion[0], self.posicion[1], self.posicion[2])
        self.gl.glScalef(ex, ey, ez)
        self.gl.glRotate(rx, 1, 0, 0)
        self.gl.glRotate(ry, 0, 1, 0)
        self.gl.glRotate(rz, 0, 0, 1)

        # creo el objeto
        es = Esfera(self.gl, self.glu)

        # Cuerpo
        es.crearEsfera(1, 20, 20, 1, 1, 1, 1, 3, 1, 0, 0, 0, 0, 0, 0)

        # brazo uno
        es.crearEsfera(1, 20, 20, 1, 1, 1, 0.5, 1.5, 0.5, 3, 0, 0, 0, 0, -60)
        es.crearEsfera(1, 20, 20, 1, 1, 1, 0.45, 1.2, 0.45, 4, 0.6, 0, 0, 0, -20)

        # brazo dos

        es.crearEsfera(1, 20, 20, 1, 1, 1, 0.5, 1.5, 0.5, -1.3, 0.4, 0, 0, 0, 45)
        es.crearEsfera(1, 20, 20, 1, 1, 1, 0.4, 1, 0.4, -4, 1.3, 0, 0, 0, 5)

        # espinas
        # espinas(self.gl,co,0,0,0,1,1,1,0,0,0)
        #
        # espinas(self.gl,co,0,-1,0,1,1,1,10,0,0)
        # espinas(self.gl, co, 0, -1, 0, 1, 1, 1, -10, 0, 0)
        #
        #
        # espinas(self.gl, co, 0, 0, 0, 1, 1, 1, 0, 180, 0)
        # espinas(self.gl, co, 0, -1, 0, 1, 1, 1, -10, 180, 0)
        # espinas(self.gl, co, 0, -1, 0, 1, 1, 1, 0, 180, 0)
        #
        # espinas(self.gl, co, 0, -1, 0, 1, 1, 1, 0, 90, 0)
        # espinas(self.gl, co, 0.5, -0.5, 0.1, 1, 1, 1, 10, 90, 0)
        # espinas(self.gl, co, 0.2, 0, 0, 1, 1, 1, 10, 90, 0)
        # espinas(self.gl, co, -0.5, -0.5, 0.1, 1, 1, 1, 0, 90, 0)
        #
        # #parte de atras
        #
        # espinas(self.gl, co, 0, -0.1,0.1, 1, 1, 1, 0, 270, 0)
        #
        # espinas(self.gl, co, 0.5, -0.3, -0.2, 1, 1, 1, 0, 270, 0)
        # espinas(self.gl, co, -0.5, -0.5, -0.2, 1, 1, 1, 0, 270, 0)
        #
        # #espinas brazos
        # espinas(self.gl, co, 1.45, -0.3, 0.1, 0.7, 0.5, 0.7, 0, 0, -25)
        # espinas(self.gl, co, 1.45, -0.3, 0.2, 0.7, 0.5, 0.7, 0, 0,-20)

        self.gl.glPopMatrix()

    def set_position(self, pos):
        self.posicion = pos

    def set_collider_position(self, pos):
        self.collider.posicion = pos

def espinas(gl, co, tx, ty, tz, ex, ey, ez, rx, ry, rz):
    gl.glPushMatrix()

    gl.glTranslate(tx, ty, tz)
    gl.glScalef(ex, ey, ez)
    gl.glRotate(rx, 1, 0, 0)
    gl.glRotate(ry, 0, 1, 0)
    gl.glRotate(rz, 0, 0, 1)

    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 2, 2, 0, 0, 0, -90, 0, False)
    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 1.8, -3, 0, 0, 0, -90, 0, False)
    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 1.5, 2, 0, 0, 0, -90, 0, False)
    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 2, 7, 0, 0, 0, -90, 0, False)
    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 1.8, 12, 0, 0, 0, -90, 0, False)
    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 1.5, 17, 0, 0, 0, -90, 0, False)
    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 1.2, 22, 0, 0, 0, -90, 0, False)
    co.crearCono(1, 1, 1, 1, 1, 0.45, 0.1, 0.1, 0.5, 27, 0, 0, 0, -90, 0, False)

    gl.glPopMatrix()


class cilindro:
    def __init__(self, radio, altura, posicion):
        self.posicion = posicion
        self.collider = ColliderElipse(radio, radio, altura, posicion)

    def draw_cylinder(self):
        glPushMatrix()
        glTranslatef(self.position[0], self.position[1], self.position[2])
        primitivas3D.draw_cylinder(0.4, 1.5)
        glPopMatrix()

    def set_position(self, pos):
        self.posicion = pos

    def set_collider_position(self, pos):
        self.collider.posicion = pos


class Terreno:

    def dibujar_terreno(self):
        s.draw(0, 2.2, 0, 55.2, 2.2, 55.2, 0, 0, 0)


class ColliderElipse:
    # definiendo un collider cilindrico en el plano xz, con altura h en el eje y
    def __init__(self, a, b, h, posicion):
        self.a = a
        self.b = b
        self.h = h
        self.posicion = posicion

    # visualizar collider
    def dibujar_collider(self):
        glPushMatrix()
        step = 0.1
        glColor3f(0.5, 0.5, 0.5)
        for ang in arange(0.0, 2 * math.pi, step):
            glBegin(GL_QUADS)
            glVertex3f(self.posicion[0] + self.a * math.cos(ang), self.posicion[1], self.posicion[2] +
                       self.b * math.sin(ang))
            glVertex3f(self.posicion[0] + self.a * math.cos(ang), self.posicion[1] + self.h, self.posicion[2] +
                       self.b * math.sin(ang))
            glVertex3f(self.posicion[0] + self.a * math.cos(ang + step), self.posicion[1] + self.h,
                       self.posicion[2] + self.b * math.sin(ang + step))
            glVertex3f(self.posicion[0] + self.a * math.cos(ang + step), self.posicion[1],
                       self.posicion[2] + self.b * math.sin(ang + step))

            glEnd()
        glPopMatrix()

    def detectar_collision(self, otroCollider):

        hayColision = False
        distanciaXZ = math.sqrt(math.pow(self.posicion[0] - otroCollider.posicion[0], 2) +
                                math.pow(self.posicion[2] - otroCollider.posicion[2], 2))
        direccion1 = [otroCollider.posicion[0] - self.posicion[0], otroCollider.posicion[2] - self.posicion[2]]
        angulo = math.acos(direccion1[0] / math.sqrt(math.pow(direccion1[0], 2) + math.pow(direccion1[1], 2)))
        vector1 = [self.a * math.cos(angulo), self.b * math.sin(angulo)]
        direccion2 = [self.posicion[0] - otroCollider.posicion[0], self.posicion[2] - otroCollider.posicion[2]]
        angulo = math.acos(direccion2[0] / math.sqrt(math.pow(direccion2[0], 2) + math.pow(direccion2[1], 2)))
        vector2 = [otroCollider.a * math.cos(angulo), otroCollider.b * math.sin(angulo)]

        distanciaMin = math.sqrt(math.pow(vector1[0], 2) + math.pow(vector1[1], 2)) + math.sqrt(
            math.pow(vector2[0], 2) + math.pow(vector2[1], 2))



        if distanciaXZ < distanciaMin-0.2:
            if self.posicion[1] < (otroCollider.posicion[1] + otroCollider.h):
                hayColision = True




        return hayColision



class Dinosaurio:
    def __init__(self, posi):
        self.chari = OBJ("OBJ/Dino3D.obj", swapyz=True)
        self.posi = posi
        self.collider = ColliderElipse(0.4, 0.8, 1, posi)

    def draw_dino(self):
        glPushMatrix()
        glTranslatef(self.posi[0], self.posi[1], self.posi[2])
        glRotatef(270, 1, 0, 0)
        glRotatef(270, 0, 0, 1)
        glScalef(0.5, 0.5, 0.5)
        glCallList(self.chari.gl_list)
        glPopMatrix()
        #self.collider.dibujar_collider()

    def set_position(self, pos):
        self.posi = pos

    def set_collider_position(self, pos):
        self.collider.posicion = pos

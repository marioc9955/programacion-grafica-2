# **************************************PROYECTO DINO3D***************
# DESARROLLADORES:
# -Cabrera Rojas Luis Mario                  lmcabrera@uce.edu.ec
# Nicolalde Perugachi Joana Estefanía        jenicolaldep@uce.edu.ec
# Rodríguez Espitia Luis Felipe              lfrodrigueze@uce.edu.ec
# Nasimba Caiza Michael Christian            mcnasimba@uce.edu.ec
# Columba Párraga Erick David                edcolumba@uce.edu.ec
# Fecha de última modificación:26/08/2020
# Descripción: Desarrollo del videojuego del dinosaurio de Chrome, en un escenario 3D
# utilizando texturas y luces

# Importación de las librerías
from pyglet.gl import glPushMatrix
import OpenGL.GL as gl
import OpenGL.GLU as glu
import pygame
# Importaciónd de clases utilizadas
from GameObjects import *
from glLibLight import *
from glLibMath import *
import Skybox as sb
import LUCES as l
from Textura import *

# Inicializacion
if sys.platform in ["win32", "win64"]: os.environ["SDL_VIDEO_CENTERED"] = "1"
pygame.mixer.pre_init(44100, -16, 2, 2048)
pygame.init()
pygame.display.init()
pygame.font.init()  # para poder escribir en pygame
# tamaño de pantalla
screen_size = (width, height) = [1280, 720]  # 800x600
screen = pygame.display.set_mode(screen_size, OPENGL | DOUBLEBUF)

glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)
glEnable(GL_DEPTH_TEST)
# Nombre para el proyecto en pygame
pygame.display.set_caption("DINO3D")
icon = pygame.image.load("recursos/icon.png")
pygame.display.set_icon(icon)
# llamamos a los sonidos que se van implementar en el juego
jump_sound = pygame.mixer.Sound('recursos/jump.wav')
die_sound = pygame.mixer.Sound('recursos/die.wav')

# definimos variables iniciales
tiempoInicial = 0
timepoFinal = 0
deltaTime = 0
t = 0
muerto = True
velocidad = 0

bo = Boton()
mitextura = Textura()  # objeto para la clase textura

pausa = False
pierde = False
inicia = True


# Metodo para inicializar variables y definir camaras
def iniciar():
    global velocidad, posicionInicialZ, posicionesInicialX, posicionesZ, obstaculos, tiempoEntreObstaculos, posXPlayer, \
        posYPlayer, puedeSaltar, posZTerreno, terreno, luces, posicionesLuces, spawnRate, postes, posicionesPostes, a, \
        contador_puntaje
    global camera_rot, camera_radius, camera_center, l
    global px, py, pz, an, al, ig
    # variables para el movimiento de cámara
    camera_rot = [90.0, 5.0]  # The spherical coordinates' angles (degrees).
    camera_radius = 3.0  # The sphere's radius
    camera_center = [0.0, 2.0, 1]  # The sphere's center
    # variables para el jugador y tiempo
    global tiempoInicial, timepoFinal, deltaTime, t, tiempoSubirDificultad, alturaMaxSalto, jugador
    tiempoSubirDificultad = 5000
    alturaMaxSalto = 2
    jugador = Dinosaurio([0, 0, 0])
    # variables para los sonidos
    global pos, pie, jump_sound, die_sound, gameO, bo
    # objetos de la clase Poste y Piedra
    pos = Poste()
    pie = Piedra()

    gameO = True
    # variables para las luces y teclado
    global x, y, z, teclado, muerto
    x = 0.7
    y = 0.7
    z = 0.7
    # permite los eventos de teclado
    teclado = pygame.key.get_pressed()
    global mitextura, pausa, pierde, inicia
    inicia = True

    # Variables para los objetos y sus posiciones
    contador_puntaje = 0
    posicionInicialZ = - 50
    posicionesInicialX = []
    posicionesZ = []
    obstaculos = []
    spawnRate = .0009
    tiempoEntreObstaculos = 1 / spawnRate
    # muerto = False;
    posXPlayer = 0
    posYPlayer = 0
    puedeSaltar = True
    posZTerreno = [0]
    terreno = [Terreno()]
    postes = []
    posicionesPostes = []

    # Creación de las luces de los postes, usando la clase glLibLigth
    luces = []
    posicionesLuces = []
    for i in range(0, 20):
        luces.append(glLibLight(4))
        signo = pow(-1, i)  # para la posición de los postes
        posicionesLuces.append([(signo * 1.8), 2.2, i * -5])
        luces[i].set_pos(posicionesLuces[i])  # determian la posición
        luces[i].set_type(GLLIB_POINT_LIGHT)  # tipos de luz
        luces[i].set_diffuse([1, 1, 0])
        luces[i].set_specular([1, 1, 0])
        luces[i].set_atten(1, 0, 1)  # atenuación de la luz
        luces[i].set_spot_dir(normalize([signo, -1, 0]))
        luces[i].set_spot_angle(45)
        luces[i].set_spot_ex(0.5)
        luces[i].enable()  # habilitamos las luces
        posicionesPostes.append([signo * 2, 0, i * -5])  # agregamos al arreglo de luces
        a = np.array(posicionesLuces)  # arreglo para la posición de los postes

    global velocidadMax, velocidadMin
    velocidadMax = 25
    velocidadMin = 7


# Método para eventos de teclado y mouse
def get_input():
    global camera_rot, camera_radius, posXPlayer, posYPlayer, puedeSaltar, muerto, velocidad
    global px, py, pz, an, al, ig, pausa, perder, inicia
    global velocidadMax, velocidadMin
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_rel = pygame.mouse.get_rel()  # Check how much the mouse moved since you last called this function.
    for event in pygame.event.get():
        # Clicked the little "X"; close the window (return False breaks the main loop).
        if event.type == QUIT:
            return False
        # If the user pressed a key:
        elif event.type == KEYDOWN:
            # If the user pressed the escape key, close the window.
            if event.key == K_ESCAPE: return False
        # If the user "clicked" the scroll wheel forward or backward:
        elif event.type == MOUSEBUTTONDOWN:
            # Zoom in
            if event.button == 4:
                camera_radius *= 0.9
            # Or out.
            elif event.button == 5:
                camera_radius /= 0.9
        elif event.type == KEYUP:
            if velocidad != 0:
                jump_sound.play()
            if event.key == K_SPACE:  # permite saltar al personaje
                puedeSaltar = False
    # If the user is left-clicking, then move the camera about in the spherical coordinates.
    # if mouse_buttons[0]:
    #   camera_rot[0] += mouse_rel[0]
    #  camera_rot[1] += mouse_rel[1]

    # Eventos para los botones
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed()

    # Area de los botones en donde se va a presionar

    # Para el boton izquierdo
    # if px + an > mouse[0] > px and py + al > mouse[1] > py:
    #   dibujarBoton(px, py, an, al,ig)
    if (mouse[0] > 170) and (mouse[0] < 420 and (mouse[1] > 590) and (mouse[1] < 680)):
        if (click[0] == 1 and muerto == True):
            # print("posición del mouse x: "+str(mouse[0]) +"posicion del mouse y:"+str(mouse[1]))
            velocidad = 10
            muerto = False
            # #Boton para comenzar desde cero
            if (pausa):
                continuar()
            else:
                iniciar()

    # actualizacion
    # Para el botón derecho
    if (mouse[0] > 850) and (mouse[0] < 1110 and (mouse[1] > 590) and (mouse[1] < 680)):
        if (click[0] == 1):
            # print("posición del mouse x: "+str(mouse[0]) +"posicion del mouse y:"+str(mouse[1]))
            if (pierde and muerto):
                return False

            else:
                pausar()  # Boton de Pausa

    # Movimiento jugador
    pressed_keys = pygame.key.get_pressed()
    # mover a la derecha e izquierda con las flechas del teclado
    if (pressed_keys[pygame.K_RIGHT] or pressed_keys[pygame.K_d]) and not muerto:
        if posXPlayer < 2:
            posXPlayer += 0.075
    if (pressed_keys[pygame.K_LEFT] or pressed_keys[pygame.K_a]) and not muerto:
        if posXPlayer > -2:
            posXPlayer -= 0.075
    # Salto del jugador, con la barra espaciadora
    if pressed_keys[pygame.K_SPACE]:
        if not muerto:
            if posYPlayer < alturaMaxSalto and puedeSaltar:
                posYPlayer += deltaTime * 3
            if posYPlayer >= alturaMaxSalto:
                puedeSaltar = False
    # letra para volver a empezar en el juego
    if pressed_keys[pygame.K_r] and muerto:
        iniciar()
    # Aumentar y disminuir la velocidad con las flechas del teclado Up y Down
    if velocidad < velocidadMax and not muerto:
        if pressed_keys[pygame.K_UP] or pressed_keys[pygame.K_w]:
            velocidad += 1  # incrementa la velocidad en una unidad hasta llegar a la velocidad maxima

    if velocidad > velocidadMin and not muerto:
        if pressed_keys[pygame.K_DOWN] or pressed_keys[pygame.K_s]:
            velocidad -= 1  # disminuye la velocidad en una unidad hasta llegar a la velocidad minima

    return True


# Metodo para posicionamiento de la camara, y cargar la matriz de identidad y perspectiva
def setup_draw():
    # Clear the screen's color and depth buffers so we have a fresh space to draw geometry onto.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    # Setup the viewport (the area of the window to draw into)
    glViewport(0, 0, screen_size[0], screen_size[1])
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, float(screen_size[0]) / float(screen_size[1]), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    camera_pos = [
        camera_center[0] + camera_radius * cos(radians(camera_rot[0])) * cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius * sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius * sin(radians(camera_rot[0])) * cos(radians(camera_rot[1]))
    ]
    gluLookAt(
        camera_pos[0], camera_pos[1], camera_pos[2],
        camera_center[0], camera_center[1], camera_center[2],
        0, 1, 0
    )


def transicionDiaNoche():
    global contador_puntaje, x, y, z
    # ***Iluminación- Transción de día a noche****
    if contador_puntaje <= 100:  # mientras el puntaje sea menor a 100 mantendremos la iluminación del día
        l.IniciarIluminacion(x, y, z)
        draw_terreno()
    else:
        if contador_puntaje <= 160:  # vamoas quitando poco a poco luz hasta llegar a 160 de puntaje
            l.IniciarIluminacion(x, y, z)
            draw_terreno()
            x -= 0.001
            y -= 0.001
            z -= 0.001
        else:
            l.IniciarIluminacion(0, 0, 0)  # finalmente quitamos la luz para la noche
            draw_terreno()


# Metodo para crear el escenario, aqui se llaman todos los elementos
def draw():
    global teclado, muerto, contador_puntaje, pausa, pierde

    glColor3f(1, 1, 1)  # da un color al escenario
    # metodo para cambiar la iluminación del escenario
    transicionDiaNoche()
    # metodo para dibujar al dinoosaurio
    draw_jugador()
    # metodo para dibujar los cactus
    draw_obstaculos()
    # metodo para dibujar las luces de los postes
    draw_luces()
    # draw_piedras()
    # dibuja los postes en el escenario
    draw_postes()
    # Escribe el puntaje en la pantalla
    drawText((1.3, 3.3, 0), "Puntaje: " + str((int)(contador_puntaje)), 32)
    # metodo para las colisiones del personaje con el cactus
    detectar_colision()
    aumentar_dificultad()
    # incremento de puntuación, sólo cuando el jugador no esté detenido
    if velocidad != 0:
        contador_puntaje = contador_puntaje + 0.1
        botones()
    else:
        if (not pausa):
            # creacion de boton Start
            bo.dibujarBoton(-0.7, 1.55, 4.3, 0.25, 0.09, mitextura.loadTexture("texturas/boton_start.jpg"))
            # creación de botón STOP
            bo.dibujarBoton(0.7, 1.55, 4.3, 0.25, 0.09, mitextura.loadTexture("texturas/boton_exit.png"))
        else:
            if (not pierde):
                botones()

    # creación de botones
    pygame.display.flip()


# Para la creación del objeto piedra (Aun no esta listo)
def draw_piedras():
    print("piedras")
    # pie.crearPiedra(2,0,0);


# Permite dibujar los postes, llamandolo de la clase Poste, lo agrega a un array de postes y los elimina
# conforme avanza el tiempo
def draw_postes():
    # global  postes, posicionesPostes,a

    a = np.array(posicionesPostes)  # posiciones de los postes
    for i in range(0, len(posicionesPostes)):
        posicionesPostes[i][2] += velocidad * deltaTime
        # postes
        postes.append(pos.crearPoste(0.1, 2.5, a[i, 0], a[i, 1], a[i, 2]))  # agrega al arreglo de postes
    # las posiciones van cambiando
    if posicionesPostes[len(posicionesPostes) - 1][2] >= -50:
        posicionesPostes.append(
            [-posicionesPostes[len(posicionesPostes) - 1][0], 0, posicionesPostes[len(posicionesPostes) - 1][2] - 5])
        b = np.array(posicionesPostes)

        postes.append(pos.crearPoste(0.1, 2.5, b[i, 0], b[i, 1], b[i, 2]))
        # 0.03 1.4
    for i in range(0, len(posicionesPostes)):
        if posicionesPostes[i][2] > 10:
            posicionesPostes.pop(i)  # elimina la posicion cuando ya no lo vemos
            postes.pop(i)  # elimina el poste, lo saca del arreglo
            break


# Al pasar el tiempo la dificualtad va incrementándose, la velocidad sube y el puntaje tambien
def aumentar_dificultad():
    global spawnRate, velocidad, t, tiempoSubirDificultad, contador_puntaje, velocidadMax, velocidadMin
    if t > tiempoSubirDificultad and not muerto:
        spawnRate += 0.0002
        velocidad += 0.5
        tiempoSubirDificultad = t + 5000
        contador_puntaje += 0.3
        velocidadMax += 0.5
        velocidadMin += 0.5


# metodo para pausar el juego
def pausar():
    global velocidad, muerto, pausa, pierde, inicia
    velocidad = 0  # detiene al personaje
    muerto = True
    pausa = True
    pierde = False
    inicia = False


# metodo para reestablecer el juego cuando se dió una pausa
def continuar():
    global velocidad, muerto, pierde, inicia
    velocidad = 10
    muerto = False
    pausa = False
    pierde = False
    inicia = False


# Metodo cuando el dinosaurio choca con un cactus
def perder():
    global muerto, pierde, pausa
    global velocidad, mitextura
    if not muerto:
        die_sound.play()  # reproducimos el sonido de muerto
    muerto = True
    pausa = False
    pierde = True
    inicia = False
    velocidad = 0  # detenemos al dinosaurio
    # dibujan en pantalla el GAME OVER (No se visualizan, por la nueva ventana de perder)
    drawText((-1.5, 2, 0), "GAME", 300)
    drawText((-1.5, 0.9, 0), "OVER", 300)
    # print("GAME OVER")#imprime en pantalla

    # Dibujamos la pantalla de GAME OVER
    bo.dibujarBoton(0, 2.1, 4, 1.8, 0.9, mitextura.loadTexture("texturas/gameover3.jpg"))
    # creación de boton START
    bo.dibujarBoton(-0.7, 1.55, 4.3, 0.25, 0.09, mitextura.loadTexture("texturas/boton_start.jpg"))
    # creación de botón STOP
    bo.dibujarBoton(0.7, 1.55, 4.3, 0.25, 0.09, mitextura.loadTexture("texturas/boton_exit.png"))


# Creacion de botones a utilizar, llamamos a la clase botones
def botones():
    # global px,py,pz,an,al,ig
    global bo, mitextura

    # creación de boton EXIT
    bo.dibujarBoton(-0.7, 1.55, 4.3, 0.25, 0.09, mitextura.loadTexture("texturas/boton_restart.png"))
    # creación de boton RESTART
    bo.dibujarBoton(0.7, 1.55, 4.3, 0.25, 0.09, mitextura.loadTexture("texturas/boton_stop.png"))


# Metodo para detectar las colisones del dino con los cactus
def detectar_colision():
    global velocidad
    for obs in obstaculos:
        if jugador.collider.detectar_collision(obs.collider):
            perder()  # llamaa al metodo perder cuando detecte la colision


# Metodo para la creación de las luces de los postes
def draw_luces():
    for i in range(0, len(posicionesLuces)):
        posicionesLuces[i][2] += velocidad * deltaTime
        luces[i].set_pos(posicionesLuces[i])

    for luz in luces:
        luz.set()
        # luz.draw_as_sphere()
        # luz.draw_as_point()
        luz.draw_as_sphe()

    if posicionesLuces[len(posicionesLuces) - 1][2] >= -50:
        luzNueva = glLibLight(4)
        signo = pow(-1, i)
        posicionesLuces.append([(-posicionesLuces[len(posicionesLuces) - 1][0]),
                                2.2, posicionesLuces[len(posicionesLuces) - 1][2] - 5])
        luzNueva.set_type(GLLIB_POINT_LIGHT)
        luzNueva.set_diffuse([1, 1, 0])
        luzNueva.set_specular([1, 1, 0])
        luzNueva.set_atten(1, 0, 1)
        luzNueva.set_spot_dir(normalize([signo, -1, 0]))
        luzNueva.set_spot_angle(45)
        luzNueva.set_spot_ex(0.5)
        luzNueva.enable()
        luces.append(luzNueva)

    for i in range(0, len(posicionesLuces)):
        if (posicionesLuces[i][2] > 10):
            posicionesLuces.pop(i)
            luces.pop(i)
            break


# Metodo para la creación del terreno
def draw_terreno():
    global posZTerreno
    # Dibuja el SkyBox
    sb.draw(0, 2.2, 0, 55.2, 10.2, 55.2, 0, 0, 0)
    # Mueve el terreno
    for i in range(0, len(posZTerreno)):
        glPushMatrix()
        glTranslatef(0, 0, posZTerreno[i])
        terreno[i].dibujar_terreno()
        glPopMatrix()

    # instanciar tereno
    if posZTerreno[len(posZTerreno) - 1] >= 0:
        terreno.append(Terreno())
        posZTerreno.append(-109)

    # mover terreno
    for i in range(0, len(posZTerreno)):
        posZTerreno[i] += velocidad * deltaTime

    # eliminar terreno lejano
    for i in range(0, len(posZTerreno)):
        if (posZTerreno[i] > 100):
            posZTerreno.pop(i)
            terreno.pop(i)
            break


# Metodo para dibujar el dinosaurio y controlar sus movimientos
def draw_jugador():
    global posXPlayer, posYPlayer, puedeSaltar

    # Controlar Caida despues de salto
    if posYPlayer > 0 and not puedeSaltar:
        posYPlayer -= deltaTime * 2.5
    if posYPlayer <= 0:
        puedeSaltar = True
        posYPlayer = 0

    # Colocar y dibujar jugador
    jugador.set_position([posXPlayer, posYPlayer, 0])
    jugador.set_collider_position([posXPlayer, posYPlayer, 0])
    jugador.draw_dino()


# Metodo para dibujar obstaculos
def draw_obstaculos():
    global deltaTime, velocidad, t, tiempoEntreObstaculos, posicionesZ, spawnRate
    # dibujar obstaculos
    for i in range(0, len(obstaculos)):
        obstaculos[i].crearCactus(0.1, 0.1, 0.12, 0, 0, 0)
        obstaculos[i].set_position([posicionesInicialX[i], 0.4, posicionesZ[i]])
        obstaculos[i].set_collider_position([posicionesInicialX[i], 0, posicionesZ[i]])
    # mover obstaculos
    for i in range(0, len(posicionesZ)):
        posicionesZ[i] += velocidad * deltaTime
    # instanciar obstaculos
    if t > tiempoEntreObstaculos and not muerto:
        obstaculos.append(Cactus(gl, glu, [0, 0.4, posicionInicialZ]))
        posicionesZ.append(posicionInicialZ)
        posicionesInicialX.append(random.random() * 4 - 2)
        tiempoEntreObstaculos = t + 1 / spawnRate
    # eliminar obstaculos lejanos
    for i in range(0, len(posicionesZ)):
        if posicionesZ[i] > 10:
            posicionesZ.pop(i)
            obstaculos.pop(i)
            posicionesInicialX.pop(i)
            break


# Metodo par ala creación de Letras en pygame
def drawText(position, textString, ta):
    font = pygame.font.SysFont("Arial", ta)
    textSurface = font.render(textString, True, (255, 255, 255, 255), (0, 0, 0, 255))
    textData = pygame.image.tostring(textSurface, "RGBA", True)
    glRasterPos3d(*position)
    glDrawPixels(textSurface.get_width(), textSurface.get_height(), GL_RGBA, GL_UNSIGNED_BYTE, textData)


# Metodo Main para correr el programa
def main():
    global tiempoInicial, tiempoFinal, deltaTime, t
    clock = pygame.time.Clock()  # reloj
    tiempoInicial = pygame.time.get_ticks()
    # Inicializamos todo el programa del dinos
    iniciar()
    # Inicializamos texturas
    sb.iniciar_texturas()
    s.iniciarTextura()
    while True:
        deltaTimeI = t
        if not get_input(): break
        setup_draw()
        draw()  # escenario
        clock.tick(60)  # Regulate the framerate to be as close as possible to 60Hz.
        tiempoFinal = pygame.time.get_ticks()
        t = tiempoFinal - tiempoInicial
        deltaTime = t - deltaTimeI
        deltaTime = deltaTime / 1000
        fps = 1 / deltaTime
        # print("fps ", fps)
        # print(t)
        # print("delta time ", deltaTime)
    pygame.quit()


if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()

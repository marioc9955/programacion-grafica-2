# DESARROLLADORES:
# -Cabrera Rojas Luis Mario                  lmcabrera@uce.edu.ec
# Nicolalde Perugachi Joana Estefanía        jenicolaldep@uce.edu.ec
# Rodríguez Espitia Luis Felipe              lfrodrigueze@uce.edu.ec
# Nasimba Caiza Michael Christian            mcnasimba@uce.edu.ec
# Columba Párraga Erick David                edcolumba@uce.edu.ec
# Fecha de última modificación:26/08/2020

from OpenGL.GL import *
import pygame

class Textura:
#   Funcion para crear texturas
    def loadTexture(self, image):
        glEnable(GL_TEXTURE_2D)
        # carga la imagen
        textureSurface = pygame.image.load(image)
        textureData = pygame.image.tostring(textureSurface, "RGBA", 1)
        texid = glGenTextures(1)  # Genera el id para la textura
        glBindTexture(GL_TEXTURE_2D, texid)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT)
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT)
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
        width, height = textureSurface.get_rect().size
        glTexImage2D(GL_TEXTURE_2D,
                     0,  # First mip-level
                     3,  # Bytes per pixel
                     width,
                    height,
                    1,  # Texture border
                    GL_RGBA,
                    GL_UNSIGNED_BYTE,
                    textureData)

        return texid
# DESARROLLADORES:
# -Cabrera Rojas Luis Mario                  lmcabrera@uce.edu.ec
# Nicolalde Perugachi Joana Estefan�a        jenicolaldep@uce.edu.ec
# Rodr�guez Espitia Luis Felipe              lfrodrigueze@uce.edu.ec
# Nasimba Caiza Michael Christian            mcnasimba@uce.edu.ec
# Columba P�rraga Erick David                edcolumba@uce.edu.ec
# Fecha de �ltima modificaci�n:26/08/2020

from OpenGL.GL import *
from OpenGL.GLU import *

def IniciarPantalla(screen):
    width = screen[0]
    height = screen[1]
    if height == 0:
        height = 1
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, 1.0 * width / height, 1.0, 10000.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

def IniciarIluminacion(x,y,z):
    glEnable(GL_BLEND)  # masking functions
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE)

    glShadeModel(GL_SMOOTH)
    glEnable(GL_TEXTURE_2D)
    # Colores de fondo
    #glClearColor(0.27, 0.45, 0.9, 0.0) # Celeste
    #glClearColor(0.5, 0.5, 0.5, 0.0) # Gris
    glClearColor(0.0, 0.0, 0.0, 0.0) # Negro
    glClearDepth(1.0)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_ALPHA_TEST)
    glDepthFunc(GL_LEQUAL)
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)

    glAlphaFunc(GL_NOTEQUAL, 0.0)

    # glFogfv(GL_FOG_DENSITY, .01)
    # glFogfv(GL_FOG_COLOR, (1,1,1,0.5))
    # glEnable(GL_FOG)

    # Par�metros de color de luz
    LightAmbient = [x, y, z, 1.0]
    LightDiffuse = [1.0, 1.0, 1.0, 1.0]
    LightIntensity = [50.0, 50.0, 50.0, 10.0]
    LightPosition = [0, 100.0, 0, 0.1]
    # Tipos de luces
    glLightfv(GL_LIGHT0, GL_AMBIENT, LightAmbient)
    glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse)
    glLightfv(GL_LIGHT0, GL_POSITION, LightPosition)
    glLightfv(GL_LIGHT0, GL_SPECULAR, LightIntensity)
    glEnable(GL_LIGHT0)
    glEnable(GL_LIGHTING)
    glEnable(GL_COLOR_MATERIAL)
#Desarrollado por Mario Cabrera

import cx_Freeze

executables = [cx_Freeze.Executable("juegoIntento.py")]
cx_Freeze.setup(name="juegoIntento",
                options={"build_exe": {"packages": ["pygame"],
                                       "include_files": ["texturas/texturaPiedra.jpg"]}},
                executables=executables
                )

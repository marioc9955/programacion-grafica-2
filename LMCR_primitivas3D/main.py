#Desarrollado por Mario Cabrera

from OpenGL.GL import *
from OpenGL.GLU import *
import pygame
from pygame.locals import *
import sys, os, traceback
from math import *
import primitivas3D
import LUCES

if sys.platform in ["win32", "win64"]: os.environ["SDL_VIDEO_CENTERED"] = "1"

pygame.display.init()
pygame.font.init()

screen_size = [800, 600]

icon = pygame.Surface((1, 1))
icon.set_alpha(0)
pygame.display.set_icon(icon)

pygame.display.set_caption("Triangles")

pygame.display.set_mode(screen_size, OPENGL | DOUBLEBUF)

glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST)
glEnable(GL_DEPTH_TEST)

camera_rot = [30.0, 20.0]  # The spherical coordinates' angles (degrees).
camera_radius = 3.0  # The sphere's radius
camera_center = [0.0, 0.0, 0.0]  # The sphere's center
LUCES.IniciarIluminacion()

def get_input():
    global camera_rot, camera_radius
    mouse_buttons = pygame.mouse.get_pressed()
    mouse_rel = pygame.mouse.get_rel()  # Check how much the mouse moved since you last called this function.
    for event in pygame.event.get():
        # Clicked the little "X"; close the window (return False breaks the main loop).
        if event.type == QUIT:
            return False
        # If the user pressed a key:
        elif event.type == KEYDOWN:
            # If the user pressed the escape key, close the window.
            if event.key == K_ESCAPE: return False
        # If the user "clicked" the scroll wheel forward or backward:
        elif event.type == MOUSEBUTTONDOWN:
            # Zoom in
            if event.button == 4:
                camera_radius *= 0.9
            # Or out.
            elif event.button == 5:
                camera_radius /= 0.9
    # If the user is left-clicking, then move the camera about in the spherical coordinates.
    if mouse_buttons[0]:
        camera_rot[0] += mouse_rel[0]
        camera_rot[1] += mouse_rel[1]
    return True


def setup_draw():
    # Clear the screen's color and depth buffers so we have a fresh space to draw geometry onto.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    # Setup the viewport (the area of the window to draw into)
    glViewport(0, 0, screen_size[0], screen_size[1])
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45, float(screen_size[0]) / float(screen_size[1]), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    camera_pos = [
        camera_center[0] + camera_radius * cos(radians(camera_rot[0])) * cos(radians(camera_rot[1])),
        camera_center[1] + camera_radius * sin(radians(camera_rot[1])),
        camera_center[2] + camera_radius * sin(radians(camera_rot[0])) * cos(radians(camera_rot[1]))
    ]
    gluLookAt(
        camera_pos[0], camera_pos[1], camera_pos[2],
        camera_center[0], camera_center[1], camera_center[2],
        0, 1, 0
    )


db = 1


def draw():
    global db
    setup_draw()
    # Start drawing lines.  Each subsequent pair of glVertex*() calls will draw one line.
    glBegin(GL_LINES)
    # Change the color to red.  All subsequent geometry we draw will be red.
    glColor3f(1, 0, 0)
    # Make two vertices, thereby drawing a (red) line.
    glVertex(0, 0, 0);
    glVertex3f(1, 0, 0)
    # Change the color to green.  All subsequent geometry we draw will be green.
    glColor3f(0, 1, 0)
    # Make two vertices, thereby drawing a (green) line.
    glVertex(0, 0, 0);
    glVertex3f(0, 1, 0)
    # Change the color to blue.  All subsequent geometry we draw will be blue.
    glColor3f(0, 0, 1)
    # Make two vertices, thereby drawing a (blue) line.
    glVertex(0, 0, 0);
    glVertex3f(0, 0, 1)
    glEnd()

    # cambiar de figura
    keypress = pygame.key.get_pressed()

    if keypress[pygame.K_1]:
        db = 1
    if keypress[pygame.K_2]:
        db = 2
    if keypress[pygame.K_3]:
        db = 3
    if keypress[pygame.K_4]:
        db = 4
    if keypress[pygame.K_5]:
        db = 5
    if keypress[pygame.K_6]:
        db = 6
    if keypress[pygame.K_7]:
        db = 7
    if keypress[pygame.K_8]:
        db = 8
    if keypress[pygame.K_9]:
        db = 9

    # Dibujar Figuras

    if (db == 1):
        primitivas3D.draw_cubo(1, 1, 0)
    if (db == 2):
        primitivas3D.draw_prisma(1, 1, 0)
    if (db == 3):
        primitivas3D.draw_piramidecuadrada(1)
    if (db == 4):
        primitivas3D.draw_sphere(0, 0, 1)
    if (db == 5):
        primitivas3D.draw_cono(0, 1, 1)
    if (db == 6):
        primitivas3D.draw_cylinder(1, 1, 1)
    if (db == 7):
        primitivas3D.draw_icosahedro(1, 0, 1)
    if (db == 8):
        primitivas3D.draw_octahedro(0, 0.5, 1)
    if (db == 9):
        primitivas3D.draw_dodecahedro( 1, 0, 0)

    pygame.display.flip()


def main():
    clock = pygame.time.Clock()
    print(glGetIntegerv(GL_MAX_TEXTURE_SIZE))
    while True:
        if not get_input(): break
        setup_draw()

        draw()
        clock.tick(60)  # Regulate the framerate to be as close as possible to 60Hz.
    pygame.quit()


if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()

# Revisado por:
# -Cabrera Rojas Luis Mario                  lmcabrera@uce.edu.ec
# Nicolalde Perugachi Joana Estefanía        jenicolaldep@uce.edu.ec
# Rodríguez Espitia Luis Felipe              lfrodrigueze@uce.edu.ec
# Nasimba Caiza Michael Christian            mcnasimba@uce.edu.ec
# Columba Párraga Erick David                edcolumba@uce.edu.ec

import sys,traceback,pygame
from OpenGL import error
class glLibError(Exception):
    def __init__(self,value):
        self.value = value
    def __str__(self):
        return repr(self.value)
def glLibTestErrors(function):
    try:
        function()
    except:
        traceback.print_exc()
        pygame.quit()
        input()
        sys.exit()
def glLibErrors(value):
    try:
        if value == True:
            error.ErrorChecker.registerChecker(None)
        elif value == False:
            error.ErrorChecker.registerChecker(lambda: None)
        elif value == GLLIB_MAX:
            error.ErrorChecker.registerChecker(None)
            logging.basicConfig(level=logging.DEBUG)
            OpenGL.FULL_LOGGING = True
    except:
        print('''Warning:
"
import OpenGL
OpenGL.ERROR_CHECKING=False
"
has been called.  glLibErrors(...) can have no effect.''')

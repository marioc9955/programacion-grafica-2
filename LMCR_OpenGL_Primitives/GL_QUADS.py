#Desarrollado por Mario Cabrera

import traceback
from GL_TRIANGLES import get_input, setup_draw
import pygame
from OpenGL.GL import *

pygame.display.set_caption("QUADS")


def quad():
    glColor3f(1, 1, 1)
    glBegin(GL_QUADS)
    glVertex3f(0, 1, 0)
    glVertex3f(1, 1, 0)
    glVertex3f(1, 0, 0)
    glVertex3f(0, 0, 0)

    glVertex3f(2, 4, 0)
    glVertex3f(4, 4, 0)
    glVertex3f(4, 2, 0)
    glVertex3f(2, 2, 0)
    glEnd()


def draw():
    quad()
    pygame.display.flip()


def main():
    clock = pygame.time.Clock()
    while True:
        if not get_input(): break
        setup_draw()
        draw()
        clock.tick(60)  # Regulate the framerate to be as close as possible to 60Hz.
    pygame.quit()


if __name__ == "__main__":
    try:
        main()
    except:
        traceback.print_exc()
        pygame.quit()
        input()